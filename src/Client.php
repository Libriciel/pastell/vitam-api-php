<?php

declare(strict_types=1);

namespace VitamClient;

use Http\Client\Common\HttpMethodsClientInterface;
use Http\Client\Common\Plugin\AddHostPlugin;
use Http\Client\Common\Plugin\HeaderDefaultsPlugin;
use Http\Client\Exception;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use VitamClient\HttpClient\Builder;
use VitamClient\HttpClient\Plugin\ExceptionThrower;

final class Client
{
    private const USER_AGENT = 'vitam-api-php';
    private Builder $httpClientBuilder;

    public function __construct(string $url = null, Builder $httpClientBuilder = null)
    {
        $this->httpClientBuilder = $httpClientBuilder ?: new Builder();
        $this->httpClientBuilder->addPlugin(new ExceptionThrower());
        $this->httpClientBuilder->addPlugin(
            new HeaderDefaultsPlugin([
                'User-Agent' => self::USER_AGENT,
            ])
        );
        if ($url) {
            $this->setUrl($url);
        }
    }

    public static function createWithHttpClient(ClientInterface $httpClient, string $url = null): self
    {
        $builder = new Builder($httpClient);
        return new self($url, $builder);
    }

    private function getHttpClientBuilder(): Builder
    {
        return $this->httpClientBuilder;
    }

    private function getHttpClient(): HttpMethodsClientInterface
    {
        return $this->getHttpClientBuilder()->getHttpClient();
    }

    private function setUrl(string $url): void
    {
        $this->getHttpClientBuilder()->removePlugin(AddHostPlugin::class);
        $this->getHttpClientBuilder()->addPlugin(
            new AddHostPlugin($this->getHttpClientBuilder()->getUriFactory()->createUri($url))
        );
    }

    /**
     * @throws Exception
     */
    public function get(string $uri, array $headers = []): ResponseInterface
    {
        return $this->getHttpClient()->get($uri, $headers);
    }

    /**
     * @throws Exception
     */
    public function postFile(string $uri, string $filePath, array $headers): ResponseInterface
    {
        return $this->getHttpClient()
            ->post(
                $uri,
                $headers,
                $this->getHttpClientBuilder()->getStreamFactory()->createStreamFromFile($filePath)
            );
    }

    /**
     * @throws Exception
     */
    public function options(string $uri, array $headers = []): ResponseInterface
    {
        return $this->getHttpClient()->options($uri, $headers);
    }

    public function ingest(): Api\Ingest
    {
        return new Api\Ingest($this);
    }
}
