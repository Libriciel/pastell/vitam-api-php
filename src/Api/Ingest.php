<?php

declare(strict_types=1);

namespace VitamClient\Api;

use Http\Client\Exception;
use VitamClient\Client;
use VitamClient\Enum\IngestAction;
use VitamClient\Enum\VitamHeaders;

final class Ingest
{
    private const INGEST_ENDPOINT = '/ingest-external/v1';
    private const INGESTS = '/ingests';
    private const ARCHIVE_TRANSFER_REPLY = '/archivetransferreply';

    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * The context is the workflow to trigger
     * @see https://www.programmevitam.fr/ressources/DocCourante/raml/externe/ingest.html#ingests_post
     *
     * @throws Exception
     * @throws \JsonException
     */
    public function create(
        int $tenantId,
        string $filePath,
        string $context,
        IngestAction $action = IngestAction::RESUME
    ): string {
        $headers = [
            VitamHeaders::X_TENANT_ID->value => $tenantId,
            VitamHeaders::X_ACTION->value => $action->value,
            VitamHeaders::X_CONTEXT_ID->value => $context,
            // This content type is required
            // @see https://github.com/ProgrammeVitam/vitam/blob/master_5.x/sources/ingest/ingest-external/ingest-external-rest/src/main/java/fr/gouv/vitam/ingest/external/rest/IngestExternalResource.java#L160=
            'Content-Type' => 'application/octet-stream',
        ];
        $response = $this->client->postFile(self::INGEST_ENDPOINT . self::INGESTS, $filePath, $headers);
        return $response->getHeaderLine(VitamHeaders::X_REQUEST_ID->value);
    }

    /**
     * The operation id is the UID received after the SIP creation (X-Request-Id response header)
     * @see https://www.programmevitam.fr/ressources/DocCourante/raml/externe/ingest.html#ingests__operationid___type__get
     *
     * @throws Exception
     */
    public function getAtr(int $tenantId, string $operationId): string
    {
        $headers = [
            VitamHeaders::X_TENANT_ID->value => $tenantId,
        ];

        $response = $this->client->get(
            self::INGEST_ENDPOINT . self::INGESTS . '/' . $operationId . self::ARCHIVE_TRANSFER_REPLY,
            $headers
        );
        return (string)$response->getBody();
    }

    /**
     * @throws Exception
     */
    public function options(int $tenantId): string
    {
        $headers = [
            VitamHeaders::X_TENANT_ID->value => $tenantId,
        ];
        $response = $this->client->options(self::INGEST_ENDPOINT, $headers);
        return (string)$response->getBody();
    }
}
