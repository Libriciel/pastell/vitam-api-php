<?php

declare(strict_types=1);

namespace VitamClient\Exception;

use Http\Client\Exception;

final class VitamException extends \Exception implements Exception
{
}
