<?php

declare(strict_types=1);

namespace VitamClient\Enum;

enum IngestAction: string
{
    case RESUME = 'RESUME';
    case NEXT = 'NEXT';
}
