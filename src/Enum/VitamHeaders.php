<?php

declare(strict_types=1);

namespace VitamClient\Enum;

enum VitamHeaders: string
{
    case X_REQUEST_ID = 'X-Request-Id';
    case X_TENANT_ID = 'X-Tenant-Id';
    case X_ACTION = 'X-Action';
    case X_CONTEXT_ID = 'X-Context-Id';
}
