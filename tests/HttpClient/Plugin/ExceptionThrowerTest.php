<?php

declare(strict_types=1);

namespace VitamClient\Tests\HttpClient\Plugin;

use GuzzleHttp\Psr7\Response;
use Http\Promise\FulfilledPromise;
use Http\Promise\Promise;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use VitamClient\Enum\VitamHeaders;
use VitamClient\Exception\VitamException;
use VitamClient\HttpClient\Plugin\ExceptionThrower;
use PHPUnit\Framework\TestCase;

final class ExceptionThrowerTest extends TestCase
{
    /**
     * @throws \JsonException
     */
    public function handleRequestProvider(): iterable
    {
        //phpcs:disable Generic.Files.LineLength
        yield '200' => [
            'response' => new Response(200),
            'exception' => null,
        ];

        yield '202' => [
            'response' => new Response(202),
            'exception' => null,
        ];

        yield '401' => [
            'response' => new Response(
                401,
                [
                    'Content-Type' => 'application/json',
                ],
                <<<EOT
{
	"Error": "X-Tenant-Id check failed!"
}
EOT
            ),
            'exception' => new VitamException(
                \json_encode([
                    'body' => [
                        'Error' => 'X-Tenant-Id check failed!',
                    ],
                ], JSON_THROW_ON_ERROR),
                401
            ),
        ];

        yield '404' => [
            'response' => new Response(
                404,
                [
                    VitamHeaders::X_REQUEST_ID->value => 'aaabbb',
                    'Content-Type' => 'application/json',
                ],
                <<<EOT
{
	"httpCode": 404,
	"code": "040102",
	"context": "External Ingest",
	"state": "Input / Output",
	"message": "Ingest external not found",
	"description": "fr.gouv.vitam.ingest.internal.common.exception.IngestInternalClientNotFoundException: Not Found Exception"
}
EOT
            ),
            'exception' => new VitamException(
                \json_encode([
                    'request_id' => 'aaabbb',
                    'body' => [
                        'httpCode' => 404,
                        'code' => '040102',
                        'context' => 'External Ingest',
                        'state' => 'Input / Output',
                        'message' => 'Ingest external not found',
                        'description' => 'fr.gouv.vitam.ingest.internal.common.exception.IngestInternalClientNotFoundException: Not Found Exception',
                    ],
                ], JSON_THROW_ON_ERROR),
                404
            ),
        ];

        yield '412' => [
            'response' => new Response(
                412,
                [
                    'Content-Type' => 'application/json',
                ],
                <<<EOT
{
	"Error": "X-Tenant-Id check failed!"
}
EOT
            ),
            'exception' => new VitamException(
                \json_encode([
                    'body' => [
                        'Error' => 'X-Tenant-Id check failed!',
                    ],
                ], JSON_THROW_ON_ERROR),
                412
            ),
        ];

        yield '415' => [
            'response' => new Response(
                404,
                [
                    VitamHeaders::X_REQUEST_ID->value => 'aaabbb',
                    'Content-Type' => 'application/json',
                ],
                <<<EOT
{
	"httpCode": 415,
	"code": "000200",
	"context": "{\"Name\":\"vitam.domain.tld\",\"Role\":\"ingest-external\",\"ServerId\":12345,\"SiteId\":1,\"GlobalPlatformId\":543210}",
	"state": "GLOBAL_INTERNAL_SERVER_ERROR",
	"message": "RESTEASY003065: Cannot consume content type",
	"description": "RESTEASY003065: Cannot consume content type"
}
EOT
            ),
            'exception' => new VitamException(
                \json_encode([
                    'request_id' => 'aaabbb',
                    'body' => [
                        'httpCode' => 415,
                        'code' => '000200',
                        'context' => "{\"Name\":\"vitam.domain.tld\",\"Role\":\"ingest-external\",\"ServerId\":12345,\"SiteId\":1,\"GlobalPlatformId\":543210}",
                        'state' => 'GLOBAL_INTERNAL_SERVER_ERROR',
                        'message' => 'RESTEASY003065: Cannot consume content type',
                        'description' => 'RESTEASY003065: Cannot consume content type',
                    ],
                ], JSON_THROW_ON_ERROR),
                404
            ),
        ];
        //phpcs:enable
    }

    /**
     * @dataProvider handleRequestProvider
     * @throws VitamException
     * @throws \JsonException
     * @throws \Exception
     */
    public function testHandleRequest(ResponseInterface $response, \Exception $exception = null): void
    {
        $requestMock = $this->createMock(RequestInterface::class);
        $promiseMock = $this->createMock(Promise::class);
        $promiseMock
            ->expects($this->once())
            ->method('then')
            ->willReturnCallback(function ($callback) use ($response) {
                return new FulfilledPromise($callback($response));
            });

        if ($exception !== null) {
            $this->expectException($exception::class);
            $this->expectExceptionCode($exception->getCode());
            $this->expectExceptionMessage($exception->getMessage());
        }

        $plugin = new ExceptionThrower();

        $fulfilledPromise = $plugin->handleRequest(
            $requestMock,
            function (RequestInterface $request) use ($promiseMock) {
                return $promiseMock;
            },
            function (RequestInterface $request) use ($promiseMock) {
                return $promiseMock;
            }
        );

        $this->assertSame($response, $fulfilledPromise->wait());
    }
}
