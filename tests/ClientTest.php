<?php

declare(strict_types=1);

namespace VitamClient\Tests;

use Http\Client\Exception;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use VitamClient\Api\Ingest;
use PHPUnit\Framework\TestCase;
use VitamClient\Client;

final class ClientTest extends TestCase
{
    private Client $client;

    protected function setUp(): void
    {
        $clientInterface = $this->createMock(ClientInterface::class);
        $this->client = Client::createWithHttpClient($clientInterface);
    }

    public function getApiClassesProvider(): iterable
    {
        yield ['ingest', Ingest::class];
    }

    /**
     * @dataProvider getApiClassesProvider
     * @phpstan-param class-string $classFqn
     */
    public function testShouldGetInstanceOfClass(string $apiName, string $classFqn): void
    {
        $this->assertInstanceOf($classFqn, $this->client->$apiName());
    }

    /**
     * @throws Exception
     */
    public function testSetUrl(): void
    {
        $clientInterface = $this->createMock(ClientInterface::class);
        $client = Client::createWithHttpClient($clientInterface, 'https://vitam.domain.tld');

        $clientInterface
            ->expects($this->once())
            ->method('sendRequest')
            ->with(
                $this->callback(function (RequestInterface $request) {
                    return $request->getUri()->getHost() === 'vitam.domain.tld';
                })
            );

        $client->ingest()->getAtr(0, 'abcd');
    }
}
