<?php

declare(strict_types=1);

namespace VitamClient\Tests\Api;

use GuzzleHttp\Psr7\Response;
use Http\Client\Exception;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Http\Client\ClientInterface;
use VitamClient\Api\Ingest;
use PHPUnit\Framework\TestCase;
use VitamClient\Client;
use VitamClient\Enum\VitamHeaders;

final class IngestTest extends TestCase
{
    /**
     * Needed for phpstan
     * @var ClientInterface|MockObject
     */
    private ClientInterface|MockObject $clientInterface;
    private Ingest $ingest;

    protected function setUp(): void
    {
        parent::setUp();
        $this->clientInterface = $this->createMock(ClientInterface::class);
        $client = Client::createWithHttpClient($this->clientInterface);
        $this->ingest = new Ingest($client);
    }

    /**
     * @throws Exception
     * @throws \JsonException
     */
    public function testCreate(): void
    {
        $expected = 'aaabbbcccddd';
        $this->clientInterface
            ->expects($this->once())
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    202,
                    [
                        VitamHeaders::X_REQUEST_ID->value => 'aaabbbcccddd',
                    ]
                )
            );

        $this->assertSame(
            $expected,
            $this->ingest->create(0, __DIR__ . '/../fixtures/sip_ok.tar', 'CONTEXT')
        );
    }

    public function atrProvider(): iterable
    {
        yield 'atr_OK' => [\file_get_contents(__DIR__ . '/../fixtures/atr_ok.xml')];
        yield 'atr_KO' => [\file_get_contents(__DIR__ . '/../fixtures/atr_ko.xml')];
    }

    /**
     * @dataProvider atrProvider
     * @throws Exception
     * @throws \JsonException
     */
    public function testGetAtr(string $atr): void
    {
        $this->clientInterface
            ->expects($this->once())
            ->method('sendRequest')
            ->willReturn(
                new Response(200, [], $atr)
            );

        $this->assertSame(
            $atr,
            $this->ingest->getAtr(0, 'aaabbbcccddd')
        );
    }

    public function testOptions(): void
    {
        $expected = '[{"permission":"ingests:create","verb":"POST","endpoint":"/ingest-external/v1/ingests/",...}]';
        $this->clientInterface
            ->expects($this->once())
            ->method('sendRequest')
            ->willReturn(
                new Response(200, [], $expected)
            );

        $this->assertSame(
            $expected,
            $this->ingest->options(0)
        );
    }
}
