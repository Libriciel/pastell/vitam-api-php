# Vitam API Php

This is based on [php-github-api](https://github.com/KnpLabs/php-github-api) by [KnpLabs](https://github.com/KnpLabs).

## Requirements

* PHP >= 8.1
* A [PSR-17 implementation](https://packagist.org/providers/psr/http-factory-implementation)
* A [PSR-18 implementation](https://packagist.org/providers/psr/http-client-implementation)


## Basic usage of `vitam-api-php` client

```php
<?php

require_once __DIR__ . '/vendor/autoload.php';

$url = 'https://vitam.domain.tld';
$certPath = '/path/to/the/certificate.p12';
$certPassphrase = 'passphrase_of_the_certificate';
$sipPath = '/path/to/my/sip.tar';
// Tenant number: X-Tenant-Id in request headers
$tenantId = 0;
// Context: X-Context-Id in request headers
$context = 'WORKFLOW';

$guzzle = new GuzzleHttp\Client([
    'cert' => [
        $certPath,
        $certPassphrase,
    ],
]);

$vitamClient = VitamClient\Client::createWithHttpClient($guzzle, $url);

$ingest = $vitamClient->ingest();

$operationId = $ingest->create($tenantId, $sipPath, $context);
$atr = $ingest->getAtr($tenantId, $operationId);
```

From `$vitamClient` object, you have access to all available Vitam api endpoints.
